import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Answer {

   public static void main (String[] param) {

      // my constants
      String hR = "---------------------------------------------------------#";
      System.out.println("Starting my program...");
      System.out.println(hR);
      // TASK 01 - Convert a given double number (double) into String (String).
       System.out.println("Task no 01:");
      String ehee = String.valueOf (Math.E);
      System.out.println ("Euler number converted to a String is: " + ehee);
      System.out.println(hR);

      // TASK 02 - Convert a string (String) into an integer number (int).
      // Assume that the string actually represents an integer
      // (Discover what happens, if the string is illegal?).
       System.out.println("Task no 02:");
      int kahtlaneNumber = 0; // init
      try {
         kahtlaneNumber = Integer.parseInt( "2019");
         System.out.print(kahtlaneNumber + " -");
      } catch (NumberFormatException e) {
         e.printStackTrace();
      }
      System.out.print("> " + (kahtlaneNumber+1) );
      System.out.println();
      System.out.println(hR);

      // TASK 03 - Take the current moment of time from the computers inner clock
      // and form a string "hours:minutes:seconds" (use 24h notation, e.g. "13:25:10").
       System.out.println("Task no 03:");
       String myTimeFormat = "HH:mm:ss";
       Date now = new Date();
       System.out.println("Java DateFormat time is: " + now);
       DateFormat df = new SimpleDateFormat(
               myTimeFormat);
       System.out
               .println("Current time in " + myTimeFormat + " formatting is: "+ df.format(now));
       System.out.println(hR);


      // TASK 04 - Find a cosine of a given angle that is expressed in degrees.
      // cos 45 deg
       System.out.println("Task no 04:");
       double myAngleInDegrees = 45;
       double cosineFromAnArgument = 0;
       try {
           cosineFromAnArgument = Math.cos(Math.toRadians(myAngleInDegrees));
       } catch (Exception e) {
           System.out
                   .println("ERROR: the trig function does not like the argument: " + myAngleInDegrees);
       }
       System.out.printf("The cosine from an %f degrees angle is: %f%n",
               myAngleInDegrees, cosineFromAnArgument);
       System.out.println(hR);


      // TASK 05 - Print a table of square roots for numbers from 0 up to 100
      // using step 5 (print two numbers per line: argument and its square root).
       System.out.println("Task no 05:");
       for (int i = 0; i < 101; i = i + 5) {
           System.out.println("Integer: " + i + " and its SQRT: "
                   + Math.sqrt((double) i));
       }


       System.out.println(hR);

      // TASK 06 - Given a string replace in it all uppercase letters with
      // corresponding lowercase letters and all lowercase letters with
      // corresponding uppercase letters. Do not change other symbols
      // ("ABcd12" -> "abCD12"). Use StringBuffer or StringBuilder class
      // for intermediate result.
       System.out.println("Task no 06:");
       String firstString = "ABcd12";
       String result = reverseCase (firstString);
       System.out.println ("\"" + firstString + "\" -> \"" + result + "\"");
       // NEW FUNCTION - for reversing a string - see BELOW
       System.out.println(hR);

      // TASK 07 - Given a string find its reverse ("1234ab" -> "ba4321").
       // Create an independent method String reverseCase (String s) for this task.
      System.out.println("Task no 07:");
       System.out.println("   Reversing the string:");
       String originalWay = "The quick brown fox jumps over the lazy dog";
       StringBuilder interMediateWay = new StringBuilder("");
       interMediateWay.append(originalWay).reverse();
       String reverseWay = interMediateWay.toString();

       System.out.println("Orig:    " + originalWay);
       System.out.println("Reverse: " + reverseWay);

      System.out.println(hR);

      // Task 08 - Given a text (String) find the number of words in it.
      // Words are separated by any positive number of any kind of whitespaces
      // (like space, tab, ...).
      // Create an independent method int countWords (String t) for this task.
       System.out.println("Task no 08:");
      String s = "How  many	 words   here";
      int nw = countWords (s);
      System.out.println (s + "\t" + String.valueOf (nw));
       System.out.println(hR);

      // TASK 09 - Register the current time, pause the program for 3 seconds,
      // register the current time again and print the difference
      // between these two moments of time in milliseconds.
      // pause. COMMENT IT OUT BEFORE JUNIT-TESTING!
       System.out.println("Task no 09:");
      long timerStart = System.nanoTime();
      try {
          Thread.sleep(3000); // mS
      } catch (InterruptedException e) {
            e.printStackTrace();
      }
      long timerStop  = System.nanoTime();
      System.out.println("Meanwhile passed: " + (timerStop - timerStart) + " nS");
      System.out.println(hR);

      // TASK 10 - Create a list (ArrayList) of 100 random integers (Integer) in between 0 and 999.
      // Print the result. What is the difference between int and Integer?
       System.out.println("Task no 10:");
       System.out
               .println("BTW: int in Java is a simple data type while Integer is an object instantiated from the Class.");
      final int LSIZE = 100;
      ArrayList<Integer> randList = new ArrayList<Integer> (LSIZE);
      Random honeyPot = new Random();
      for (int i=0; i<LSIZE; i++) {
         randList.add (Integer.valueOf (honeyPot.nextInt(1000)));
      }
       System.out.println ("Before reverse:  " + randList);
       reverseList (randList);
       System.out.println(hR);

      // TASK 11 - Find the minimal element of this list.
      // How to find the minimal element of any List of Comparable elements
      // (read about interfaces, also consider that ArrayList implements
      // List and Integer implements Comparable)?
       System.out.println("Task no 11:");
       int theMinimalMember = Collections.min(randList).intValue();
       System.out.printf("Working with a list of random integers, of size: %5d,%n", randList.size() );
       System.out.printf("   and this was the least value: %4d%n", theMinimalMember);
       System.out.println(hR);


      // TASK 12 - Reverse the order of elements of the list you have
      // created before. Write an independent method to reverse any list.
       System.out.println("Task no 12:");
       System.out.println ("After reverse: " + randList);
       System.out.println(hR);


      // TASK 13 - Write an independent method to find the maximal
      // element of any Java collection (you can use your list for
      // testing).
       System.out.println("Task no 13:");
       System.out.println ("Maximum: " + maximum (randList));
       System.out.println(hR);

       // TASK 14 - Create a hashtable (HashMap) containing 5 pairs of strings,
       // where "subject code" serves as a key and "subject name" serves as
       // a value (e.g. ("I231", "Algorithms and Data Structures") , just
       // choose any subjects you like).


       // HashMap tasks:
       //    14.1 create
       System.out.println("Subtask no 14-1: - create a hashtable");
       Hashtable<String, String> courseName = new Hashtable<String, String>(); // Java 8 ????
       courseName.put("ICS0005", "Algoritmid ja andmestruktuurid");
       courseName.put("IDC0007", "Veebitehnoloogiad");
       courseName.put("ICY0024", "Hulgad, seosed, süsteemid");
       courseName.put("ICY0030", "Kõrgem matemaatika");
       courseName.put("ICY0006", "Tõenäosusteooria");
       System.out.println(courseName.size() + " elements created (key, value)");
       System.out.println(hR);

       //    14.2 print all keys
       System.out.println("Subtask no 14-2: print all the keys");
       Enumeration<String> keyString = courseName.keys();
       while (keyString.hasMoreElements()) {
           String nextKey = (String) keyString.nextElement();
           System.out.println("\t" + nextKey);
       }
       System.out.println(hR);

       //    14.3 remove a key
       System.out.println("Subtask no 14-3: removing a key - ICY0006");
       courseName.remove("ICY0006");
       System.out.println(courseName.size() + " elements remaining");
       System.out.println(hR);

       //    14.4 print all pairs
       System.out.println("Subtask no 14-4:");
       Enumeration<String> yetAnotherKeystring = courseName.keys();
       // yetAnotherKeystring = courseName.keys();
       while (yetAnotherKeystring.hasMoreElements()) {
           String nextKey = (String) yetAnotherKeystring.nextElement();
           String nextValue = (String) courseName.get(nextKey); // Kurat, seda saab ilusamaks teha
           System.out.println("\t" + nextKey + " - " + nextValue);
       }
       System.out.println(hR);

   }

   /** Finding the maximal element.
    * @param a Collection of Comparable elements
    * @return maximal element.
    * @throws NoSuchElementException if <code> a </code> is empty.
    */
   static public <T extends Object & Comparable<? super T>>
         T maximum (Collection<? extends T> a) 
            throws NoSuchElementException {
      return Collections.max(a);
   }

   /** Counting the number of words. Any number of any kind of
    * whitespace symbols between words is allowed.
    * @param text text
    * @return number of words in the text
    */
   public static int countWords (String text) {
      return new StringTokenizer(text).countTokens();
   }

   /** Case-reverse. Upper -> lower AND lower -> upper.
    * @param s string
    * @return processed string
    */
   public static String reverseCase (String s) {
        StringBuilder sb = new StringBuilder();
        for (char ch: s.toCharArray()) {
            if (Character.isLowerCase(ch)) {
                sb.append(Character.toUpperCase(ch));
            } else if (Character.isUpperCase(ch)) {
                sb.append(Character.toLowerCase(ch));
            } else {
                sb.append(ch);
            }
       }

      return sb.toString();
   }

   /** List reverse. Do not create a new list.
    * @param list list to reverse
    */
   public static <T extends Object> void reverseList (List<T> list)
      throws UnsupportedOperationException {
         Collections.reverse(list);
   }
}
